![UNLAM - Universidad Nacional de La Matanza](https://www.grupolarabida.org/wp-content/uploads/2020/10/Argentina_UniversidadNacionaldeLaMatanza_UNLAM_02_.jpg)
# Administracion de Consorcios

**ASP.NET MVC - .NET Framework 4.8
Entity Framework**

Trabajo Práctico para Programación Web 3 - 2020 2º Cuatrimestre
## Integrantes del Grupo:
  - Fortunato Juan Pablo - @juampifortu
  - Vazquez Claudio - @Gallo-Claudio
  - Zota Alejo - @AlejoZ

### Notas sobre la app
Scripts utilizados y librerias

> **Para la paginación:** Se implementa el script **"[https://datatables.net/](https://datatables.net/)"**
> **Para la notificación:** Se implementa el script **"[https://sweetalert2.github.io/](https://datatables.net/)"**
> **Para el breadcrumbs:** Se implementa la librería **"[https://github.com/maartenba/MvcSiteMapProvider)"**
